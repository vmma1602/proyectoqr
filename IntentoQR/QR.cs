﻿using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using BarcodeLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Drawing.Drawing2D;
using iTextSharp.text.pdf;
using iTextSharp;
using iTextSharp.text;
using Exportar_PDF_Excel;
namespace IntentoQR
{
public partial class QR : Form
{
        Exportar_Archivos exportar = new Exportar_Archivos();
        string letras = "";
     
    
        public QR()
        {
           InitializeComponent();
        }

        private void QR_Load(object sender, EventArgs e)
        {
            
        }
        public void GenerarQR(string texto, PictureBox pic, TextBox textbox)
        {
            // Los codigos QR que se generan pueden guardar 100 caracteres, incluyendo espacios
            // Si se necesita guardar mas se configura el objeto Bitmap que se llama imagen
            //Es importante que sean palabras clave para que al imprimir no salga incompleto


            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = new QrCode();
            qrEncoder.TryEncode(texto, out qrCode);

            GraphicsRenderer renderer = new GraphicsRenderer(new FixedCodeSize(200, QuietZoneModules.Zero), Brushes.Black, Brushes.White);

            MemoryStream ms = new MemoryStream();

            renderer.WriteToStream(qrCode.Matrix, ImageFormat.Png, ms);
            var imageTemporal = new Bitmap(ms);
            var imagen = new Bitmap(imageTemporal, new Size(new Point(100, 100)));
            pic.Image = imagen;


            // Guardar en el disco duro la imagen (Carpeta del proyecto)
            imagen.Save("Codigo.png", ImageFormat.Png);
         
            textbox.Text = txtCodigo.Text;





        }
        public Bitmap GuardarPanelImg(Panel p)
        {
            Bitmap bmp = new Bitmap(p.Width, p.Height);
            p.DrawToBitmap(bmp, new System.Drawing.Rectangle(0, 0, p.Width, p.Height));
            return bmp;
        }
        private void BtnGenerar_Click(object sender, EventArgs e)
        {
           // Los codigos QR que se generan pueden guardar 100 caracteres, incluyendo espacios
           // Si se necesita guardar mas se configura el objeto Bitmap que se llama imagen
           //Es importante que sean palabras clave para que al imprimir no salga incompleto


            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = new QrCode();
            qrEncoder.TryEncode(txtCodigo.Text, out qrCode);

            GraphicsRenderer renderer = new GraphicsRenderer(new FixedCodeSize(200, QuietZoneModules.Zero), Brushes.Black, Brushes.White);

            MemoryStream ms = new MemoryStream();

            renderer.WriteToStream(qrCode.Matrix, ImageFormat.Png, ms);
            var imageTemporal = new Bitmap(ms);
            var imagen = new Bitmap(imageTemporal, new Size(new Point(100,100)));           
            pictureBox1.Image = imagen;
      

            // Guardar en el disco duro la imagen (Carpeta del proyecto)
            imagen.Save("imagen.png", ImageFormat.Png);
          
            textBox1.Text = txtCodigo.Text;           
           





        }

        
        private void BtnGuardar_Click(object sender, EventArgs e)
        {

            //Esto una vez que se tiene ya una imagen final 
            System.Drawing.Image imgFinal = (System.Drawing.Image)GuardarPanelImg(panelQR);   
            

            
                        SaveFileDialog CajaDeDialogoGuardar = new SaveFileDialog();
                        CajaDeDialogoGuardar.AddExtension = true;
                        CajaDeDialogoGuardar.Filter = "Image PNG (*.png)|*.png";
                        CajaDeDialogoGuardar.ShowDialog();
                        if (!string.IsNullOrEmpty(CajaDeDialogoGuardar.FileName))
                        {
                           imgFinal.Save(CajaDeDialogoGuardar.FileName, ImageFormat.Png);

                        }
                        imgFinal.Dispose();
                        
          
        }
      
        public string GuardarPDF()
        {
            //Esto una vez que se tiene Doc final final 
            using (SaveFileDialog CajaDeDialogoGuardar = new SaveFileDialog())
            {
                CajaDeDialogoGuardar.AddExtension = true;
                CajaDeDialogoGuardar.Filter = "Archivos PDF (*.pdf)|*.pdf";
                CajaDeDialogoGuardar.ShowDialog();

                CajaDeDialogoGuardar.FileName.ToString();

                return CajaDeDialogoGuardar.FileName.ToString();   //Retorna un string con el nombre y ruta del archivo
            }

        }
        public void DrawRoundRect(Graphics g, Pen p, float X, float Y, float width, float height, float radius)
        {
            //Se manda llamar este metodo en el evento Paint de algun control, en este caso un panel
            GraphicsPath gp = new GraphicsPath();
            //Upper-right arc:
            gp.AddArc(X + width - (radius * 2), Y, radius * 2, radius * 2, 270, 90);
            //Lower-right arc:
            gp.AddArc(X + width - (radius * 2), Y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
            //Lower-left arc:
            gp.AddArc(X, Y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
            //Upper-left arc:
            gp.AddArc(X, Y, radius * 2, radius * 2, 180, 90);
            gp.CloseFigure();
            g.DrawPath(p, gp);
            gp.Dispose();
        }
        private void PanelQR_Paint(object sender, PaintEventArgs e)
        {
            Graphics v = e.Graphics;
            DrawRoundRect(v, Pens.Gray, e.ClipRectangle.Left, e.ClipRectangle.Top, e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1, 5);
            base.OnPaint(e);
                   
        }
        void RedondearPanel(PaintEventArgs e)
        {
            Graphics v = e.Graphics;
            DrawRoundRect(v, Pens.Gray, e.ClipRectangle.Left, e.ClipRectangle.Top, e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1, 5);
            base.OnPaint(e);
        }
        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
        
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            exportar.LevantarActa(GuardarPDF(), "Levantar acta", "dfsdsf", "Victor","Jose");
        }
    }
    }

