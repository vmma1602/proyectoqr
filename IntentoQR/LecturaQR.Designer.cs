﻿namespace IntentoQR
{
    partial class LecturaQR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLectura = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtLectura
            // 
            this.txtLectura.Location = new System.Drawing.Point(21, 76);
            this.txtLectura.Name = "txtLectura";
            this.txtLectura.Size = new System.Drawing.Size(266, 20);
            this.txtLectura.TabIndex = 0;
            this.txtLectura.TextChanged += new System.EventHandler(this.TxtLectura_TextChanged);
            this.txtLectura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtLectura_KeyPress);
            // 
            // LecturaQR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 180);
            this.Controls.Add(this.txtLectura);
            this.Name = "LecturaQR";
            this.Text = "LecturaQR";
            this.Load += new System.EventHandler(this.LecturaQR_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.LecturaQR_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLectura;
    }
}